const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Designer = mongoose.model("Designer");
const Client = mongoose.model("Client");
const Login = mongoose.model("admin");

const multer = require("multer");
const path = require("path");

const bodyparser = require("body-parser");
router.use(
    bodyparser.urlencoded({
        extended: true,
    })
);

// For saving the picture of designer in a folder
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "images/");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});

let upload = multer({ storage: storage });

// Home Page Route.
router.get("/", (req, res) => {
    Designer.find((err, data) => {
        if(!err) {
            console.log(data);

            res.render("homepage", {
                title: "JerryPearl Decors",
                designerList: data,
            });
        } else {
            console.error(err);
            res.render('/error');
        }
    }).lean();
});


// Login Page Route
router.get("/login", function(req, res) {
    res.render("login", {
        title: "Login @JerryPearls",
    });
});

// Designer Register Page Route
router.get("/registerDesigner", function(req, res) {
    res.render("designerRegister", {
        title: "Register Designer @JerryPearls",
    });
});

// Register New Designer
router.post("/designer", upload.single("desImage"), (req, res) => {
    registerNewDesigner(req, res);
});

// Function to save Designer's data
function registerNewDesigner(req, res) {
    des = new Designer();

    des.name = req.body.dName;
    des.designsFor = req.body.designs;
    des.contact = req.body.dPhone;
    des.email = req.body.dEmail;
    des.location = req.body.dLocation;
    des.remarks = req.body.dRemarks;
    des.desImage = req.file.filename;

    // console.log("Printing Designer data => " + des);
    des.save((err, doc) => {
        if(!err) {
            console.log("Designer Registered Successfully.");
            res.render("registerDes", {
                title: "Registration Successful @JerryPearl",
            });
        } else {
            // console.log(err);
            res.redirect('/error');
        }
    })

}

// Id to save client's request for respective designer
let id;
router.get("/reg/:id", (req, res) => {
    // console.log(req.params);
    id = req.params.id;
    Designer.findOne({ _id: id }, function(err, dt) {
        if(!err) {
            res.render("clientRegister", {
                title: "Book Request @JerryPearls",
            });
        } else {
            res.render('pageNotFound');
        }
    })
});

// Client Registration POST Route - Save Client's data
router.post("/reg/des/client", function(req, res) {
    let desId = id;
    // console.log(desId);
    let user = new Client();

    // Fetching Designer's record to save respective details in client's record
    Designer.findOne({ _id: id }, function(err, dt) {
        if(!err) {
            user.name = req.body.cName;
            user.contact = req.body.cPhone;
            user.email = req.body.cEmail;
            user.location = req.body.cLocation;
            user.remarks = req.body.cRemarks;
            user.des_int = desId;
            user.des_name = dt.name;
            user.des_for = dt.designsFor;

            // console.log(user);
            user
                .save()
                .then(() => {
                    console.log("Client Registered Successfully.");
                })
                .catch(() => {
                    console.log("Couldn't Save Your Request!");
                    res.render("errorPage");
                });
        }
    });
    res.render("clientRequest", {
        title: "Request Sent @JerryPearls",
    });
});

// Admin Page Login Authentication
router.post("/admin", async(req, res) => {
    let usernm = req.body.uname;
    let passwd = req.body.pass;
    if(usernm != "" && passwd != "") {
        const chkLogin = await Login.findOne({
            $and: [{ username: usernm }, { password: passwd }],
        });

        if(chkLogin) {
            console.log("Authentication Success.");
            loadAdminPage(req, res);
        } else {
            console.log("Authentication Failed.");
            res.render("login", { errMsg: "Login Failed!!! Try Again." });
            // res.redirect('/error')
        }
    }
});

// Function to load Admin dashboard if authentication is successful 
function loadAdminPage(req, res) {
    Client.find({ reqStatus: false }, (err, data) => {
        if(!err) {
            // console.log(data);
            res.render("adminDashboard", {
                title: "Admin Dasboard @JerryPearls",
                clients: data
            });
        } else {
            console.error(err);
            res.render('errorPage');
        }
    }).lean();
}

// Update collection if client has been contacted by the designer
router.post("/contactClient", function(req, res) {
    let content = "";
    req.on("data", (data) => {
        content = content + data;
        // console.log(content);
        Client.updateOne({ _id: { $eq: content } }, { reqStatus: true },
            function(err, data) {
                if(!err) {
                    console.log("Record Updated.");
                } else {
                    res.redirect('/error');
                }
            }
        );
    });
    res.send();
});

// Page Unavailable Route.
router.get('/:name', (req, res) => {
    // console.log(req.params);
    res.render("pageNotFound")
});

module.exports = router;