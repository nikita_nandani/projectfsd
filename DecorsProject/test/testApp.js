const assert = require('assert');
const request = require('supertest');
const app = require('../app');
const db = require('../models/login');
const dbDes = require('../models/desSch');


// Test for Home Page Status
describe('Unit Testing the home page', function () {

    it('Should return OK status', function () {
        return request(app)
            .get('/')
            .then(function (response) {
                assert.equal(response.status, 200);
                // console.log(response);
            })
    });
});

// Test for Client Registration Page Status
describe('Unit Testing "Client Request To Designer" Route', function () {

    it('Should return OK status', function () {
        return request(app)
            .get('/reg/:id')
            .then(function (response) {
                assert.equal(response.status, 200);
                // console.log(response);
            })
    });
});

// Test for Login Page Status
describe('Unit Testing Login Page Route', function () {

    it('Should return OK status', function () {
        return request(app)
            .get('/login')
            .then(function (response) {
                assert.equal(response.status, 200);
                // console.log(response);
            })
    });
});

// Test for Mongo DB connection
describe('Reading Details of Admin from Database', () => {
    it('Finds Admin', (done) => {
        db.findOne({ username: 'adminNikita' })
            .then((user) => {
                // console.log(user);
                assert(user.username === 'adminNikita');
                done();
            });
    })
})

// Designer ID valid test for client registration page
describe('Reading Details of Designer from Database', () => {
    let id = 'falseID';
    // a valid id copied from database
    let trueID = '61934f82aab3a68105e1a76c';
   
    it('Does not Finds Designer', (done) => {
        dbDes.findOne({ _id: id }, (err) => {
            if(err) {
                done();
            }
        })
    })

    it('Finds Designer', (done) => {
        dbDes.findOne({ _id: trueID })
            .then((designer) => {
                assert(designer._id == trueID);
                done();
            })
            // .catch((reject) => console.log(reject));
    })
})

// Test for Authentication - Login Post Page 
describe('Unit Test for Admin Login Success', function () {

    it('Should return OK status', function () {
        return request(app)
            .post('/admin')
            .then(function (response) {
                assert.equal(response.status, 200);
                // console.log(response);
            })
    });
});

// Test for Admin Dashboard Status
describe('Unit Testing ADMIN Dashboard Route', function () {

    it('Should return OK status', function () {
        return request(app)
            .get('/admin')
            .then(function (response) {
                assert.equal(response.status, 200);
                // console.log(response);
            })
    });
});


// Test for contact client page
describe('Unit Testing for ajax request /contactClient From Admin Dashboard', function () {

    it('Should return OK status', function () {
        return request(app)
            .get('/contactClient')
            .then(function (response) {
                assert.equal(response.status, 200);
                // console.log(response);
            })
    });
});

// Test for Designer Registration Page Status
describe('Unit Testing Designer Registration Page Status', function () {

    it('Should return OK status', function () {
        return request(app)
            .get('/registerDesigner')
            .then(function (response) {
                assert.equal(response.status, 200)
                // console.log(response);
            })
    });
});

// Test for 404_not_found pages 
describe('Unit Testing PAGE_NOT_FOUND Routes', function () {

    it('Should return OK status', function () {
        return request(app)
            .get('/not_a_valid_route')
            .then(function (response) {
                assert.equal(response.status, 200);
                // console.log(response);
            })
    });
});

// Test for validation on Client Side
describe('Client Side Validation Testing', function () {
    let name = "Nikita Nandani";
    let falseName = "Nikita123";

    let mail = "nikita@gmail.com";
    let falseMail = "nikita.gmail.com";

    let phone = "9874563210";
    let falsePhone1 = "9874563"
    let falsePhone2 = "nikita's phone"

    let checkName = /^[a-zA-Z ]{2,30}$/gi;
    let checkMail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    let checkPhone = /\D/gi;    //not number regex

    it('Should return true', function () {
        console.log('Name, Phone Number, Mail ID Check');
        assert.equal(checkName.test(name), true);
        assert.equal(!checkPhone.test(phone), true);
        assert.equal(checkMail.test(mail), true);
    });

    it('Should return false', function () {
        console.log('False Name , Phone Number and Mail ID Check');
        assert.equal(checkName.test(falseName), false);
        assert.equal((falsePhone1.length === 10), false);
        assert.equal(!checkPhone.test(falsePhone2), false);
        assert.equal(checkMail.test(falseMail), false);
    });
})