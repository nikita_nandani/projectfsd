// Designer Registration Page Validation
let designerForm = document.getElementById("designerForm");
designerForm.addEventListener("submit", validate, false);

function validate(evt) {
  let nm = document.getElementById("dName").value;

  let chk1 = document.getElementById("dDesHome");
  let chk2 = document.getElementById("dDesOffc");
  let chk3 = document.getElementById("dDesParty");
  let chk4 = document.getElementById("dDesEvent");
  let des = [];
  if(chk1.checked) {
    des.push(chk1.value);
  }
  if(chk2.checked) {
    des.push(chk2.value);
  }
  if(chk3.checked) {
    des.push(chk3.value);
  }
  if(chk4.checked) {
    des.push(chk4.value);
  }

  let mob = document.getElementById("dPhone").value;
  let mail = document.getElementById("dEmail").value;

  let checkName = /^[a-zA-Z ]{2,30}$/gi;

  let checkMail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  let checkPhone = /\D/gi;
  if(!checkName.test(nm) || nm === "") {
    evt.preventDefault();
    alert("Please enter alphabets in your Name");
    document.getElementById("dName").value = "";
    document.getElementById("dName").focus();
  } else if(des.length === 0) {
    evt.preventDefault();

    alert("Please select atleast one design areas.");
    document.getElementById("dDesHome").focus();
  } else if(checkPhone.test(mob) || mob.length !== 10) {
    evt.preventDefault();
    alert("Please enter valid Phone Number");

    document.getElementById("dPhone").value = "";
    document.getElementById("dPhone").focus();
  } else if(!checkMail.test(mail) || mail.length === 0) {
    evt.preventDefault();
    alert("Please enter Valid Email");
    document.getElementById("dEmail").value = "";
    document.getElementById("dEmail").focus();
  }
}




///////////////

// let nameField = document.getElementById('dName');
// nameField.addEventListener('keypress', checkName, false);

// function checkName(evt) {
//   let charCode = evt.charCode;
//   if(charCode != 0) {

//     if(charCode < 97 || charCode > 122) {
//       evt.preventDefault();
//       displayWarning(
//         "Please use alphabets only."
//         + "\n" + "charCode: " + charCode + "\n"
//       );
//     }
//   }
// }
// let warningTimeout;
// let warningBox = document.createElement("div");
// warningBox.className = "warning";

// function displayWarning(msg) {
//   warningBox.innerHTML = msg;

//   if(document.body.contains(warningBox)) {
//     window.clearTimeout(warningTimeout);
//   } else {
//     // insert warningBox after nameField
//     nameField.parentNode.insertBefore(warningBox, nameField.nextSibling);
//   }

//   warningTimeout = window.setTimeout(function() {
//       warningBox.parentNode.removeChild(warningBox);
//       warningTimeout = -1;
//     }, 2000);
// }

// ////////////

// //////////////////////////////////////
