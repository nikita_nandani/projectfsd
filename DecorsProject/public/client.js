// Client Page Validation
let clientForm = document.getElementById("clientForm");
clientForm.addEventListener("submit", validate, false);

function validate(evt) {
  let nm = document.getElementById("cName").value;
  let mob = document.getElementById("cPhone").value;
  let mail = document.getElementById("cEmail").value;

  let checkName = /^[a-zA-Z ]{2,30}$/gi;

  let checkMail =/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  let checkPhone = /\D/gi;
  if(!checkName.test(nm) || nm === "") {
    evt.preventDefault();
    alert("Please enter alphabets in your Name");
    document.getElementById("cName").value = "";
    document.getElementById("cName").focus();
  } else if(checkPhone.test(mob) || mob.length !== 10) {
    evt.preventDefault();
    alert("Please enter valid Phone Number");

    document.getElementById("cPhone").value = "";
    document.getElementById("cPhone").focus();
  } else if(!checkMail.test(mail) || mail.length === 0) {
    evt.preventDefault();
    alert("Please enter Valid Email");
    document.getElementById("cEmail").value = "";
    document.getElementById("cEmail").focus();
  }
}
