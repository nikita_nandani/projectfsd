// Admin Page Requests

// AJAX request sending for client contact updation
function clientContacted(clientId) {
  // console.log(clientId);
  let xhr = new XMLHttpRequest();
  let url = "/contactClient";

  const confirm = window.confirm("Is the client contacted?");

  if(confirm) {
    document.getElementById(clientId).remove();
    checkLists();
    xhr.open("POST", url, true);
    xhr.send(clientId);
  } else {
    return;
  }
}

// Function to remove the client data from browser display if the client is contacted by the designer
function checkLists() {
  let row = document.getElementsByTagName("tr");
  console.log(row.length);
  if(row.length === 1) {
    document.getElementById("clientTable").style.visibility = "hidden";

    document.getElementById("noRecords").style.visibility = "visible";
    document.getElementById("noRecords").style.display = "block";
  }
}
