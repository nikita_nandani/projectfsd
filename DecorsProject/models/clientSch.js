const mongoose = require("mongoose");

//Client Schema
const clientSchema = new mongoose.Schema({
    date: { type: Date, default: Date.now },
    name: String,
    contact: Number,
    email: String,
    location: String,
    remarks: { type: String, default: "None" },
    des_int: String,
    des_name: String,
    des_for: Array,
    reqStatus: { type: Boolean, default: false },
});

//Client Model
module.exports = mongoose.model("Client", clientSchema);