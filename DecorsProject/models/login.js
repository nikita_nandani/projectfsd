const mongoose = require("mongoose");

// Login Schema
const loginSchema = new mongoose.Schema({
    username: String,
    password: String,
});

module.exports = mongoose.model("admin", loginSchema);