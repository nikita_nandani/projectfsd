const mongoose = require("mongoose");

// Designer Schema
const designerSchema = new mongoose.Schema({
    date: { type: Date, default: Date.now },
    name: String,
    designsFor: [{ type: String }],
    contact: Number,
    email: String,
    location: String,
    remarks: { type: String, default: "None" },
    desImage: { type: String },
});

//Designer Model
module.exports = mongoose.model("Designer", designerSchema);