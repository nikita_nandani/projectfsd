const mongoose = require("mongoose");
mongoose.set("runValidators", true);

mongoose.connect(
    "mongodb://localhost:27017/decors", { useNewUrlParser: true },
    (err) => {
        if(!err) {
            console.log("MongoDB Connection Succeeded");
        } else {
            console.log("Error in DB connection : " + err);
        }
    }
);

require("./desSch");
require("./clientSch");
require("./login");