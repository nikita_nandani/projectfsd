require('./models/db');
const express = require("express");
const expresshbs = require('express-handlebars');
const path = require('path');
const app = express();
const router = require('./controllers/router')

app.set('view engine', 'hbs');
app.use(express.urlencoded({
    extended: true
}));

app.use(express.static(path.join(__dirname, "public")));

app.set('views', path.join(__dirname, '/views/'));
app.engine('hbs', expresshbs({ extname: 'hbs', defaultLayout: 'basicLayout', layoutsDir: __dirname + '/views/layouts/' }));
app.use(express.static('images'))

const PORT = 3050;
module.exports = app.listen(PORT, (err) => {
    if (err) {
        console.log("Error connecting to server")
    }
    else {
        console.log("Server running at: " + PORT);
    }
})

app.use('/', router)
